Title: Montana de Oro Beach
----
Published: 26 April 2011
----
Tags: Nature
----
Text:

Montana de Oro is a small California State Park in Los Osos. It is one of my favorite places because it has a beach, campgrounds, sand dunes with horse trails, and fun winding roads. It's usually overcast there but with beautiful greenery and yellow flowers near the seaside cliffs. This photo was a personal project to test a new lens.
