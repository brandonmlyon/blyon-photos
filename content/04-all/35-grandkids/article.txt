Title: Grandkids
----
Published: 26 April 2011
----
Tags: Portraits
----
Text:

There's nothing like a good old fashioned large family portrait to add some importance to a portfolio. It's always hard to photograph a large group all smiling and not blinking. I love how colorful and cheery this group's clothing is with the verdant background. This photo took place outdoors during the summer in California.
