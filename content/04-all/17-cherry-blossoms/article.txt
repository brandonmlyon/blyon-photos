Title: Cherry Blossoms
----
Published: 26 April 2011
----
Tags: Nature
----
Text:

Every year sometime in January the cherry trees blossom in San Luis Obispo. The first few sunny days of winter bring some great photographic opportunities. A simple distraction-free background can completely change the subject in the photo.
