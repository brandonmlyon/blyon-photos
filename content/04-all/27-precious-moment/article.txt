Title: Precious Moment
----
Published: 26 April 2011
----
Tags: Weddings
----
Text:

A flower garden in the background softened the mood to go along with the family moment. The event's time of year meant the flowers were in bloom. This was taken at a wedding at the Madonna Inn in San Luis Obispo California. Lighting was nice and even despite being outdoors, thanks to the overcast sky. I am super happy with the way this photo turned out.
