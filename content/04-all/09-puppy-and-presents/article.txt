Title: Puppy and Presents
----
Published: 26 April 2011
----
Tags: Pets
----
Text:

This puppy was a world of fun to photograph. This was taken during a product photoshoot for a clothing company in San Luis Obispo, CA. Sales were boosted by having a cute Christmas puppy next to the clothes. Pets rarely hold still and this one was no exception. For this session I brought along lighting, backdrops, and props. This was one of my favorite resulting photos, due to the way the puppy is eyeing the present with wonder since it was his first Christmas.
