Title: The Avila Life
----
Published: 31 Jul 2013
----
Tags: 
----
Text:

This classic car is parked in front of a subtle but lovely sunset. At the beach in the summer. With plenty of boats floating by in the background. To me this picture is an epitome of coastal California living. It's the prelude to a beach bonfire with friends. I want to be there right now. Capturing a moment in time which you want to live in forever is an ultimate goal of a photographer. The only cliches missing are palm trees.
