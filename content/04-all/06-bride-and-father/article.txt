Title: Bride and Father
----
Published: 24 Feb 2011
----
Tags: Weddings
----
Text:

This beautiful rose garden was a great place for an outdoor California wedding.  A photo of walking the bride down the aisle. A side view with the bouquet as a focal point. The father and bride captured alone going forward together. This is one of my favorite photo types. It provides a great framed memory for both people involved. I used a wide open lens to capture all of the detail and a medium shutter speed to stop the movement.
