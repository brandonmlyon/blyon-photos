Title: Cat Eyes
----
Published: 23 Feb 2011
----
Tags: Pets
----
Text:

As with most pets, it was difficult to get this one to hold still, look at the camera, and allow me to get close enough for the detail I needed. This client in Misson Canyon in Santa Barbara, CA requested that I get a close-up shot of the animal's eyes and face. Thanks to outdoor overcast lighting I managed to get a crisp and clear shot, even though his fur patterns are fairly intricate.
