Title: Ring and Bouquet
----
Published: 26 April 2011
----
Tags: Weddings
----
Text:

Every bride should have a picture of their ring and bouquet positioned over their dress. This was taken using low, soft, and diffuse lighting. That helped to brighten the flowers and ring while darkening the less important bits. This photo was taken at a wedding in San Luis Obispo, California.
