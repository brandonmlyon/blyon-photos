Title: Santa Barbara Courthouse Wedding
----
Published: 15 Sept 2012
----
Tags: Weddings
----
Text:

In order to get a photo of the entire wedding party I had to use a wide angle lens. Fortunately this meant there was also room to capture much of the historical courthouse in the background. Unfortunately the edges blur with this type of lens but the focus is on the bride and groom anyways. This photo looks even better when viewed as a panoramic. This wedding ceremony took place late in the day during the end of the summer in California.
