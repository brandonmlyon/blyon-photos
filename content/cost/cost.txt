Title: Cost
----
Description: Cost
----
Text:

As a young but experienced photographer with low operating costs I can offer you a much more reasonable price than most. **You will never be charged for something we didn't discuss in advance or for something you didn't approve**. If you aren't happy with the results we can discuss and may reschedule for no additional cost.

#### Plan 1 ####

This plan is most beneficial to clients with little money to invest up-front. On this plan you pay no costs of any kind until you decide to buy prints or rights at a later date.

* An initial set of 4x6 prints from the session will cost $1 per photo. This is a good tool to help decide what photos or digital negatives you might want to purchase. Alternately you can browse a set of low-quality photos online for free for that same purpose.
* On this plan the photographer retains ownership of the digital negatives and an exclusive reproduction license (unless these are purchased at a later date by the original client for a reasonable fee). Digital files are otherwise unavailable to the client.
* When clients decide to purchase prints the cost per photo in this package will be much more than in Plan2 in order to make up for the investment costs on the photographer's end. As an example, an 8x10 might cost $20 per print instead of the $5 they might cost on Plan2.

#### Plan 2 ####

This plan is most beneficial to clients with a large wishlist. You pay a small shooting session fee up-front. With each photographic session you will receive for no extra fee:

* One set of 4x6 prints in an album
* 10 of your favorite photos enlarged to 8x10
* A limited quantity of free photographic touch-ups
* Your own copy of the digital negatives *(most photographers would NEVER give you this)*
* (optional) Your photos uploaded to a website of your choice
* (optional) Extra prints are available with no cost markup
