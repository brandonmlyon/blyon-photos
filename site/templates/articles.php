<?php snippet('header') ?>

<?php snippet('navigation') ?>

<!--Article-->
<div class="grid">
	<?php if(param('tag')) {
	
	  $articles = $pages->find('all')
	                    ->children()
	                    ->visible()
	                    ->filterBy('tags', param('tag'), ',')
	                    ->flip()
	                    ->paginate(1);
	
	} else {
	
	  $articles = $pages->find('all')
	                    ->children()
	                    ->visible()
	                    ->flip()
	                    ->paginate(1);
	
	} ?>

	<div class="row article-image">
		<div class="slot-6-7-8-9">
			<?php foreach($articles as $article): ?>
				<?php if ($article->hasImages()) : ?>
					<span class="pagination">
						<?php if($articles->pagination()->hasPrevPage()): ?>
							<a class="prev" href="<?= $articles->pagination()->prevPageURL() ?>"><span>&lt;</span></a>
						<?php endif ?>
						<?php if($articles->pagination()->hasNextPage()): ?>
							<a class="next" href="<?= $articles->pagination()->nextPageURL() ?>"><span>&gt;</span></a>
						<?php endif ?>
					</span>
					<img src="<?php echo $article->images()->first()->url() ?>" />
				<?php endif ?>
			<?php endforeach ?>
		</div>
	</div>
	<div class="row">
		<div class="slot-6-7-8-9">
			<?php foreach($articles as $article): ?>
				<h1 class="article-title"><a href="<?php echo $article->url() ?>"><?php echo html($article->title()) ?></a></h1>
			<?php endforeach ?>
		</div>
	</div>
	<div class="row article-content">
		<div class="slot-6-7-8-9">
			<?php foreach($articles as $article): ?>
				<?php echo kirbytext($article->text()) ?>
			<?php endforeach ?>
		</div>
	</div>

	<?php if($articles->pagination()->hasPages()): ?>
	<div class="row">
		<div class="slot-6-7-8-9">
			<div class="pagination">
				<?php if($articles->pagination()->hasPrevPage()): ?>
				<a class="prev" href="<?= $articles->pagination()->prevPageURL() ?>">« Previous</a>
				<?php endif ?>
				
				<?php if($articles->pagination()->hasPrevPage() && $articles->pagination()->hasNextPage()): ?>
				|
				<?php endif ?>
				
				<?php if($articles->pagination()->hasNextPage()): ?>
				<a class="next" href="<?= $articles->pagination()->nextPageURL() ?>">Next  »</a>
				<?php endif ?>
			</div>
		</div>
	</div>
	<?php endif ?>
</div>

<?php snippet('footer') ?>