<!--Navigation-->
<div class="header">
	<ul id="navigation">
		<?php foreach($pages->visible() AS $p): ?>
		<li><a href="<?php echo $p->url() ?>" class="nav <?php echo html($p->title()) ?>"><?php echo html($p->title()) ?></a></li>
		<?php endforeach ?>
		<?php $tagcloud = tagcloud($pages->find('all'), array()) ?>
		<?php foreach($tagcloud as $tag): ?>
			<li><a href="<?php echo $tag->url() ?>" class="nav"><?php echo $tag->name() ?></a></li>
		<?php endforeach ?>
	</ul>
</div>
<hr />
