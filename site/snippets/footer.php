<!--Footer-->
<div class="footer">
	<p><?php echo html($site->copyright()) ?></p>
</div>

<!--jQuery Plugin-->
<?php echo js('assets/js/jquery-1.7.1.min.js') ?>

<!-- TinyNav.js -->
<?php echo js('assets/js/tinynav.js') ?>
<script>
	//<![CDATA[
	$(function () {	
		$('#navigation').tinyNav({
			header: true
		});
	});
	//]]>
</script>

<!-- Enable CSS active pseudo styles in Mobile Safari -->
<script language="JavaScript">
	//<![CDATA[
		document.addEventListener("touchstart", function() {},false);
	//]]>
</script>

</body>
</html>
